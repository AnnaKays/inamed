import IMask from 'imask';

class PhoneMask {
  #elements;

  constructor(elements) {
    this.#elements = elements;
    this.maskOptions = {
      mask: '+{7} (900) 000-00-00',
      lazy: false,
    };
    // this.init();
  }

  init() {
    this.#elements.forEach((element) => new IMask(element, this.maskOptions));
  }
}

export const phoneMask = new PhoneMask(document.querySelectorAll('input[type="tel"]'));
