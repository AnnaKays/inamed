import media from '../../common/js/modules/match-media';

const initMap = () => {
  const map = document.getElementById('home-contacts-map');
  if (!map) return;

  // eslint-disable-next-line no-undef
  const myMap = new ymaps.Map(map, {
    center: [55.706945, 37.47],
    controls: ['zoomControl'],
    zoom: 15,
  }, {
    searchControlProvider: 'yandex#search',
    autoFitToViewport: 'always',
  });
  // myMap.behaviors.disable('scrollZoom');
  // myMap.behaviors.disable('drag');
  // myMap.behaviors.disable('multiTouch');

  // Создание точки с дефолтной иконкой
  // eslint-disable-next-line no-undef
  const myPlacemark = new ymaps.Placemark([55.706944, 37.481864], {
    // Данные для отображения во всплывающем окне
    balloonContent: 'ул. Веерная, д. 1, к. 2',
  }, {
    // Опции иконки
    preset: 'islands#icon', // Стиль иконки
    iconColor: '#2c66bc', // Цвет иконки
  });

  // Добавление точки на карту
  myMap.geoObjects.add(myPlacemark);

  const matchMediaMap = () => {
    media.addChangeListener('max', 'm', matchMediaMap);
    if (media.max('m')) myMap.setCenter([55.706944, 37.481864]);
    else myMap.setCenter([55.706945, 37.47]);
  };

  matchMediaMap();
};

class HomeContacts {
  #element;
  #isYandexMapShowed;

  constructor(element) {
    this.#element = element;
  }

  #scrollHandler = () => {
    if (!this.#isYandexMapShowed) {
      // eslint-disable-next-line no-undef
      ymaps.ready(initMap);
      this.#isYandexMapShowed = true;
      window.removeEventListener('scroll', this.#scrollHandler);
    }
  };

  init = () => window.addEventListener('scroll', this.#scrollHandler);
}

export const homeContacts = new HomeContacts(document.querySelector('.home-contacts'));
