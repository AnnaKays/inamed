class Header {
  #element;
  // #switcher;
  // #activeClass = 'header--active';
  #scrolledClass = 'header--scrolled';

  constructor(element) {
    this.#element = element;
    // this.#switcher = this.#element.querySelector('.js-header-switcher');
  }

  #onScrolled = () => {
    if (window.scrollY > 0) this.#element.classList.add(this.#scrolledClass);
    else this.#element.classList.remove(this.#scrolledClass);
  };

  init = () => {
    document.addEventListener('scroll', this.#onScrolled);
  };
}

export const header = new Header(document.querySelector('.header'));
