class MatchMedia {
  #media = {}; // приватные свойства и методы, их нельзя получить через точку, нельзя вызвать
  #onChangeHandlers = {};
  #sizes = {
    s: 480,
    m: 800,
    l: 1000,
    xl: 1200,
  };

  #setMedia = () => {
    Object.entries(this.#sizes).forEach((size) => {
      this.#media[size[0]] = {
        min: window.matchMedia(`(min-width: ${size[1]}px)`),
        max: window.matchMedia(`(max-width: ${size[1] - 1}px)`),
      };
      this.#onChangeHandlers[size[0]] = {
        min: [],
        max: [],
      };
    });
  };

  min = (size) => this.#media[size].min.matches;
  max = (size) => this.#media[size].max.matches;

  // eslint-disable-next-line max-len
  addChangeListener = (type, size, func) => this.#onChangeHandlers[size][type].push(func); // вызывается метод, через метод передаем параметры

  #setListener = (size, type) => this.#onChangeHandlers[size][type].forEach((handler) => handler());

  init = () => {
    this.#setMedia();
    Object.entries(this.#media).forEach((media) => {
      media[1].min.addEventListener('change', () => this.#setListener(media[0], 'min'));
      media[1].max.addEventListener('change', () => this.#setListener(media[0], 'max'));
    });
  };
}

const media = new MatchMedia(); // передаем в скобочки данные
media.init(); // это обычный объект с методами

export default media;
