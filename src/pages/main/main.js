import { accordionItem } from '../../blocks/accordion-item/accordion-item';
import { HomeTreatment } from '../../blocks/home-treatment-item/home-treatment-item';
import { phoneMask } from '../../blocks/form/form';
import { homeContacts } from '../../blocks/home-contacts/home-contacts';

document.addEventListener('DOMContentLoaded', () => {
  accordionItem();
  const homeGallery = document.querySelector('.home-gallery .swiper');
  if (homeGallery) {
    const homeGallerySlider = new window.Slider(homeGallery, {
      spaceBetween: 32,
      breakpoints: {
        320: {
          slidesPerView: 1,
        },
        480: {
          slidesPerView: 2,
        },
        800: {
          slidesPerView: 3,
        },
      },
    });
    homeGallerySlider.init();
  }

  // eslint-disable-next-line no-new
  new HomeTreatment();
  phoneMask.init();
  homeContacts.init();
});
