import { header } from '../../blocks/header/header';
import Slider from '../../blocks/slider/slider';

window.Slider = Slider;

document.addEventListener('DOMContentLoaded', () => {
  header.init();
});
